/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import React from "react";

const OurProcess = ({ ourProcess }) => {
  const {
    paragraphOne,
    paragraphTwo,
    paragraphThree,
    paragraphFour,
    paragraphFive,
    paragraphSix,
    paragraphSeven,
    subheading,
  } = ourProcess;

  const { largeHeaderProcess, smallHeaderProcess } =
    ourProcess.sectionHeaderProcess;

  const bulletPoints = ourProcess?.bulletPointsProcess;
  const columnImage = ourProcess?.leftColumnImage?.sourceUrl;

  return (
    <section
      className="container mx-auto relative px-9 lg:px-0 mb-24"
      id="our-process-section">
      <div className="container">
        <div className="grid grid-flow-col grid-cols-1 md:grid-cols-2 gap-4">
          <div>
            <img src={columnImage} />
          </div>
          <div>
            <p className="content-small-header">{smallHeaderProcess}</p>
            <p className="content-large-header">{largeHeaderProcess}</p>
            <p className="content-text">{paragraphOne}</p>
            <p className="content-text">{paragraphTwo}</p>
            <p className="content-text">{paragraphThree}</p>
            <p className="content-text">{paragraphFour}</p>
            <ul className="bullet-list">
              {bulletPoints.map((bulletPoint) => {
                const { bulletTextProcess } = bulletPoint;
                return (
                  <li className="text-bullet content-text">
                    {bulletTextProcess}
                  </li>
                );
              })}
            </ul>
            <p className="content-text">{paragraphFive}</p>
            <p className="subheading">{subheading}</p>
            <p className="content-text">{paragraphSix}</p>
            <p className="content-text">{paragraphSeven}</p>
          </div>
        </div>
      </div>

      <p className="mb-8 text-lg font-opensans text-payline-black">
        {/* {ssubheadline} */}
      </p>
    </section>
  );
};

export default OurProcess;
