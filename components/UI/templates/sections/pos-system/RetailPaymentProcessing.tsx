/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import React from "react";

const RetailPaymentProcessing = ({ retailPaymentProcessing }) => {
  const {
    paragraphOne,
    paragraphTwo,
    paragraphThree,
    paragraphFour,
    paragraphFive,
    paragraphSix,
    paragraphSeven,
    subheading,
  } = retailPaymentProcessing;

  const { largeHeaderRetail, smallHeaderRetail } =
    retailPaymentProcessing.sectionHeaderRetail;

  const bulletPoints = retailPaymentProcessing?.bulletPointsRetail;
  const columnImage = retailPaymentProcessing?.rightColumnImage?.sourceUrl;

  return (
    <section
      className="container mx-auto relative px-9 lg:px-0 mb-24"
      id="ratail-payment-processing-section">
      <div className="container">
        <div className="grid grid-flow-col grid-cols-1 md:grid-cols-2 gap-4">
          <div>
            <p className="content-small-header">{smallHeaderRetail}</p>
            <p className="content-large-header">{largeHeaderRetail}</p>
            <p className="content-text">{paragraphOne}</p>
            <p className="content-text">{paragraphTwo}</p>
            <p className="content-text">{paragraphThree}</p>
            <p className="content-text">{paragraphFour}</p>
            <br />
            <ul className="bullet-list">
              {bulletPoints.map((bulletPoint) => {
                const { bulletTextRetail } = bulletPoint;
                return (
                  <li className="text-bullet content-text">
                    {bulletTextRetail}
                  </li>
                );
              })}
            </ul>
            <p className="content-text">{paragraphFive}</p>
            <p className="subheading">{subheading}</p>
            <p className="content-text">{paragraphSix}</p>
            <p className="content-text">{paragraphSeven}</p>
          </div>
          <div>
            <img src={columnImage} />
          </div>
        </div>
      </div>

      <p className="mb-8 text-lg font-opensans text-payline-black">
        {/* {ssubheadline} */}
      </p>
    </section>
  );
};

export default RetailPaymentProcessing;
