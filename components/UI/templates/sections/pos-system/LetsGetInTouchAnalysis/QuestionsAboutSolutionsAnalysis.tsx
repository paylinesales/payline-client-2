import React from "react";
import ColorTextBox from "@/components/UI/atoms/ColorTextBox";

const QuestionsAboutSolutionsAnalysis: React.FC = () => {
  return (
    <div className="w-full lg:w-6/12 lg:px-12">
      <h3 className="text-lg font-semibold text-payline-black mb-6 text-center">
        <span>Receive a</span>
        <ColorTextBox className="pos-color-free">free</ColorTextBox>
        <br />
        <span>cost analysis</span>
      </h3>
      <img
        src="/images/svg/curly-arrow-down.svg"
        className="get-in-touch-pos-upper"
        width={190}
        height={88}
      />
      <div className="w-6/12 lg:w-5/12 mx-auto">
        <img
          src="/images/svg/sales-team.svg"
          className="w-full"
          width={190}
          height={88}
        />
        <div className="font-bold text-2xl text-payline-black text-center mt-2">
          Sales Team
        </div>
        <div className="text-center text-payline-dark mb-2">Online now</div>

        <img
          src="/images/svg/arrow-down-black-thin.svg"
          className="get-in-touch-pos-lower mx-auto"
          width={8}
          height={22}
        />
        <a
          href="/signup-today"
          type="button"
          className="w-full border border-solid border-payline-border-light rounded-full bg-payline-black text-white px-5 py-2 text-center">
          SCHEDULE A CALLBACK
        </a>
      </div>
    </div>
  );
};

export default QuestionsAboutSolutionsAnalysis;
