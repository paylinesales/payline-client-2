/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import React from "react";
import Link from "@/components/UI/atoms/LinkComponent";

const BetterWaySendMoney = ({ betterWaySendMoney }) => {
  const {
    paragraphTwoRightColumn,
    paragraphTwoLeftColumn,
    paragraphThreeRightColumn,
    paragraphThreeLeftColumn,
    paragraphOneRightColumn,
    paragraphOneLeftColumn,
  } = betterWaySendMoney;

  const { largeHeaderMoney, smallHeaderMoney } =
    betterWaySendMoney.sectionHeaderMoney;

  const bulletPoints = betterWaySendMoney?.bulletPointsLeftColumnMoney;

  return (
    <section
      className="container mx-auto relative px-9 lg:px-0 mb-24"
      id="better-way-send-money-section">
      <div className="content-square-background container">
        <p className="content-small-header">{smallHeaderMoney}</p>
        <p className="content-large-header">{largeHeaderMoney}</p>
        <div className="grid grid-flow-col grid-cols-1 md:grid-cols-2 gap-4 pos-solutions-first">
          <div className="wrapper-text-area">
            <p className="content-text">{paragraphOneLeftColumn}</p>
            <p className="content-text">{paragraphTwoLeftColumn}</p>

            <ol className="num-list">
              {bulletPoints?.map((bulletPoint) => {
                const { bulletTextMoney } = bulletPoint;
                return (
                  <li className="text-bullet content-text">
                    {bulletTextMoney}
                  </li>
                );
              })}
            </ol>

            <p className="content-text">{paragraphThreeLeftColumn}</p>
          </div>
          <div className="wrapper-text-area">
            <p className="content-text">{paragraphOneRightColumn}</p>
            <p className="content-text">{paragraphTwoRightColumn}</p>
            <p className="content-text">{paragraphThreeRightColumn}</p>
          </div>
        </div>
        {/* TODO set link location */}
        <Link
          href="#"
          className="sales-button rounded-sm bg-payline-dark font-bold text-payline-white py-2 px-6 border-2 border-payline-black -ml-2 text-sm lg:text-lg hover:bg-payline-black transition-all">
          Contact sales
        </Link>
        <img className="mt-20" src="/images/target-pointer.png" />
      </div>

      <p className="mb-8 text-lg font-opensans text-payline-black">
        {/* {ssubheadline} */}
      </p>
      {/* {bulletPoints.map((bulletPoint) => {
                        const { bulletText } = bulletPoint;
                        return (
                            <p className="content-text">
                                {bulletText}
                            </p>
                        );
                    })} */}
    </section>
  );
};

export default BetterWaySendMoney;
