import React from "react";
import SectionTitle from "components/UI/atoms/SectionTitle";
import { useBusinessCategory } from "context/BusinessCategoryContext";
import InfoBox from "@/components/UI/molecules/InfoBox";
import { PAYMENT_TYPES } from "../../../../../constants";

const { ONLINE } = PAYMENT_TYPES;
const cards = [
  {
    imageSrc: [
      "/images/svg/vt-dashboard.svg",
      "/images/svg/vt-dashboard-green.svg",
    ],
    title: "VT/Dashboard",
    description: "All your dashboard needs covered in one place",
    link: "#",
  },
  {
    imageSrc: [
      "/images/svg/cart-integration.svg",
      "/images/svg/cart-integration-green.svg",
    ],
    title: "Cart Integration & Payment Page",
    description: "All your dashboard needs covered in one place",
    link: "#",
  },
  {
    imageSrc: ["/images/svg/invoicing.svg", "/images/svg/invoicing-green.svg"],
    title: "Invoicing & Recurring Billing",
    description: "All your dashboard needs covered in one place",
    link: "#",
  },
  {
    imageSrc: ["/images/svg/api.svg", "/images/svg/api-green.svg"],
    title: "API and Developer Docs",
    description: "All your dashboard needs covered in one place",
    link: "#",
  },
];

const SolutionsTailoredToBusinessSection: React.FC = () => {
  const { paymentType } = useBusinessCategory();

  // for as long as the payment type is on line then the business is online
  const isOnlineBusiness = paymentType === ONLINE;

  return (
    <section
      id="solutions-tailored-to-business-section"
      className="container mx-auto my-12 py-10 px-8 md:px-16 lg:px-20 xl:px-9">
      <SectionTitle
        subtitle="What's Included"
        firstPartOfHeadline="Solution"
        highlighedText="tailored"
        lastPartOfHeadline="to your business"
      />
      <div className="flex flex-wrap w-full xs:w-4/5 mx-auto mt-28 xs:my-14 justify-center gap-28 xs:gap-7">
        {cards.map((card) => (
          <InfoBox
            className="w-full lg:w-5/12"
            imageSrc={isOnlineBusiness ? card.imageSrc[1] : card.imageSrc[0]}
            title={card.title}
            description={card.description}
            link={card.link}
          />
        ))}
      </div>
    </section>
  );
};

export default SolutionsTailoredToBusinessSection;
