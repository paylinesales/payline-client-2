/* eslint-disable react/no-array-index-key */
import React, { ReactElement, useState } from "react";
import Image from "next/image";
import Link from "next/link";
import posts from "data/fakePosts";
import SearchInput from "../../../atoms/SearchInput";

const BlogPost = ({ post }) => {
  return (
    <div className="bg-white py-5 flex flex-col rounded-lg shadow-lg overflow-hidden w-96 ">
      <div className="flex-shrink-0">
        <img className="h-48 w-full object-cover" src={post.imageUrl} alt="" />
      </div>
      <div className="flex-1 bg-white p-6 flex flex-col justify-between">
        <div className="flex-1">
          <Link href={post.href}>
            <div className="block mt-2">
              <p className="text-xl font-semibold text-gray-900 line-clamp-3">
                {post.title}
              </p>
            </div>
          </Link>
        </div>
      </div>
      <div>
        <Link href={post.href}>
          <div className="flex gap-3 text-payline-black font-bold px-6">
            <div>Read</div>
            <Image
              src="/images/svg/chevron-right-black.svg"
              width={8}
              height={15}
            />
          </div>
        </Link>
      </div>
    </div>
  );
};

const BlogPosts = () => {
  const [index, setIndex] = useState(0);
  const onDotClick = (idx) => {
    return () => {
      if (posts[idx]) {
        setIndex(idx);
      }
    };
  };

  return (
    <>
      <div className="mx-6 my-3">RECENT ARTICLES</div>
      <div className="my-3 flex w-56">
        <BlogPost post={posts[index]} />
      </div>
      <div className="flex justify-center gap-2 mx-2 my-12">
        {[...Array(Math.min(posts.length, 5)).fill(undefined)].map((_, i) => {
          if (index === i) {
            return (
              <Image
                key={i}
                src="/images/svg/payline-dot-black.svg"
                width={14}
                height={14}
                onClick={onDotClick(i)}
              />
            );
          }
          return (
            <Image
              key={i}
              src="/images/svg/payline-dot-gray.svg"
              width={10}
              height={10}
              onClick={onDotClick(i)}
            />
          );
        })}
      </div>
    </>
  );
};

function RecentArticlesSectionMobile(): ReactElement {
  return (
    <>
      <div className="md:hidden bg-blog bg-no-repeat bg-cover">
        <div className="my-12 max-w-7xl flex justify-center">
          <p className="text-4xl font-hkgrotesk leading-tight font-bold text-payline-black">
            The
            <span className="text-4xl px-2 font-hkgrotesk leading-tight font-bold text-payline-white bg-payline-cta">
              Blog
            </span>
          </p>
        </div>

        <div className="flex items-center justify-center">
          <SearchInput />
        </div>
        <BlogPosts />
      </div>
    </>
  );
}

export default RecentArticlesSectionMobile;
