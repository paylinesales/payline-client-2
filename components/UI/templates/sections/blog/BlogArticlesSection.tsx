/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import React, { useState } from "react";
import moment from "moment";
import client from "lib/apolloClient";
import Image from "next/image";
import { ChevronRightIcon, ArrowDownIcon } from "@heroicons/react/outline";
import Link from "next/link";
import { blogPageQuery } from "@/queries/blog";
import WhiteButton from "@/components/UI/atoms/WhiteButton";

const BlogArticlesSection = ({ postsData }) => {
  const { edges, pageInfo } = postsData;
  const [blogPosts, setBlogPosts] = useState(edges);
  const [nextPageInfo, setNextPageInfo] = useState(pageInfo);

  const handleLoadMore = async () => {
    const { data } = await client.query({
      query: blogPageQuery,
      variables: {
        first: 5,
        after: nextPageInfo.endCursor,
      },
    });

    const { posts } = data;
    posts.edges.map((post) =>
      setBlogPosts((prevBlogPosts) => [...prevBlogPosts, post]),
    );
    setNextPageInfo(posts.pageInfo);
  };

  return (
    <div className="my-12 md:px-20">
      {blogPosts.map((post) => {
        return (
          <>
            <div className="flex my-5">
              <div className="w-full md:w-6/10">
                <div className="px-4 flex gap-2">
                  <Image src="/images/svg/ach.png" width={16} height={16} />
                  <p className="text-orange">
                    {moment(post.node.date).format("DD MMMM YYYY")}
                  </p>
                </div>

                <div className="md:hidden">
                  <div className="px-2 flex items-center gap-3 p-3">
                    <div className="mx-3">
                      <a href={post.node.author.node.url || "#"}>
                        <img
                          className="h-10 w-10 rounded-full"
                          src={
                            post.node.author.node.avatar.url ||
                            "https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
                          }
                          alt=""
                        />
                      </a>
                    </div>
                    <div className="font-bold">
                      <span>
                        {`${post.node.author.firstName} ${post.node.author.lastName}` ||
                          "Payment Expert"}
                      </span>
                    </div>
                  </div>
                </div>

                <h1 className="px-4 text-4xl text-payline-black">
                  <b>{post.node.title}</b>
                </h1>
                <div
                  // eslint-disable-next-line react/no-danger
                  dangerouslySetInnerHTML={{ __html: post.node.excerpt }}
                  className="px-4 my-6"
                />
                <div className="flex px-4 my-6">
                  {post.node.categories.edges.map((category) => (
                    <WhiteButton
                      disabled={false}
                      onClick={() => {
                        // eslint-disable-next-line no-console
                        console.log("clicked");
                      }}
                      className="rounded-full font-bold">
                      {category.node.name}
                    </WhiteButton>
                  ))}
                </div>
                <div className="flex items-center px-4 my-6">
                  {/* {console.log(`/blog/${post.node.slug}`)} */}
                  <Link href={`/blog/${post.node.slug}`}>
                    <a
                      // onClick={onClick}
                      className="cursor-pointer flex gap-2 items-center text-payline-black font-bold px-6">
                      <span>Read</span>
                      <ChevronRightIcon className="w-5 h-5" />
                    </a>
                  </Link>
                </div>

                <div className="block mx-0 px-0 md:hidden">
                  <img
                    src={
                      post.node.featuredImage !== null
                        ? post.node.featuredImage.node.mediaItemUrl
                        : "https://images.unsplash.com/photo-1496128858413-b36217c2ce36?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1679&q=80"
                    }
                  />
                </div>
              </div>
              <div className="hidden md:block pt-4">
                <div className="flex items-center gap-3 p-3">
                  <div className="mx-3">
                    <a href={post.node.author.node.url}>
                      <img
                        className="h-10 w-10 rounded-full"
                        src={post.node.author.node.avatar.url}
                        alt=""
                      />
                    </a>
                  </div>
                  <div>
                    <b>
                      {post.node.author.firstName
                        ? `${post.node.author.firstName} ${post.node.author.lastName}`
                        : "Payment Expert"}
                    </b>
                  </div>
                </div>
                <img
                  src={
                    post.node.featuredImage !== null
                      ? post.node.featuredImage.node.mediaItemUrl
                      : "https://images.unsplash.com/photo-1496128858413-b36217c2ce36?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1679&q=80"
                  }
                  className="w-64 p-3"
                />
              </div>
            </div>
          </>
        );
      })}
      <div className="flex w-full justify-center">
        <button
          type="button"
          onClick={handleLoadMore}
          className="w-full flex justify-center items-center gap-2">
          <span className="text-payline-black font-bold text-lg">
            Load More
          </span>
          <ArrowDownIcon className="w-5 h-5" />
        </button>
      </div>
    </div>
  );
};

export default BlogArticlesSection;
