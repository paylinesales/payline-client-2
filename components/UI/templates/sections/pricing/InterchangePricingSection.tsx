import React, { ReactElement } from "react";
import PricingCard from "./interchangePricingSection/PricingCard";
import TitleAndSupport from "./interchangePricingSection/TitleAndSupport";
import { PAYMENT_TYPES } from "../../../../../constants";

const { ONLINE, IN_PERSON } = PAYMENT_TYPES;

function InterchangePricingSection(): ReactElement {
  return (
    <>
      <div className="bg-pricing bg-cover bg-bottom bg-no-repeat px-5">
        <div className="container flex-none pt-16 mx-auto">
          <TitleAndSupport />
          <div className="container -mb-24">
            <div className="grid grid-cols-2 gap-10">
              <PricingCard
                className="col-span-2 lg:col-span-1"
                ImageSrc="/images/swipe-in-person.png"
                cardTitle="Swiping In-Person"
                cardPercentage={0.2}
                businessType={IN_PERSON}
              />
              <PricingCard
                className="col-span-2 lg:col-span-1"
                ImageSrc="/images/card-not-present.png"
                cardTitle="CREDIT CARDS NOT PRESENT PLAN"
                cardPercentage={0.4}
                businessType={ONLINE}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default InterchangePricingSection;
