import { ReactElement } from "react";
import HalfCircle from "@/components/UI/atoms/Icons/HalfCircle";
import SearchInput from "@/components/UI/atoms/SearchInput";
import CategoriesPicker from "../../../atoms/CategoriesPicker";

const SearchBox = ({
  setCategories,
  categories,
  search,
  setSearch,
  querySetters,
  queryResult,
}): ReactElement => {
  const { loading, error, data } = queryResult;
  return (
    <div className="py-3">
      <div className="flex gap-3 items-center">
        <HalfCircle />
        <div className="text-payline-peach uppercase">Search Results</div>
      </div>

      <div className="py-3">
        <div className="flex items-center justify-between flex-wrap gap-3">
          <SearchInput
            categories={categories}
            search={search}
            setSearch={setSearch}
            querySetters={querySetters}
          />
        </div>
        <div className="pt-3 flex items-left justify-between flex-wrap gap-3">
          <CategoriesPicker
            categories={categories}
            setCategories={setCategories}
          />
        </div>
      </div>
      {(loading || error || data) && (
        <div className="px-2 text-2xl text-payline-black font-bold">
          Results for "Search"
          {/* Search will be contextual based on the query */}
        </div>
      )}
    </div>
  );
};

export { SearchInput, SearchBox as default };
