import Image from "next/image";
import React, { ReactElement } from "react";
import Link from "next/link";
import GoogleIcon from "../../../atoms/Icons/GoogleIcon";
import InstagramIcon from "../../../atoms/Icons/InstagramIcon";
import LinkedInIcon from "../../../atoms/Icons/LinkedInIcon";
import TwitterIcon from "../../../atoms/Icons/TwitterIcon";
import ToggleWhiteBox from "../../../atoms/ToggleWhiteBox";

const caseStudies = [
  "Medical",
  "OmniChannel",
  "Hub and Spoke",
  "Franchises",
  "High Risk",
  "eCommerce",
];

const MobileLinks = (): ReactElement => {
  return (
    <div className="gap-5 mx-6">
      <ToggleWhiteBox title="Case Study" key={0}>
        {caseStudies.map((c) => {
          return (
            <p key={c}>
              <Link href="#">{c}</Link>
            </p>
          );
        })}
      </ToggleWhiteBox>
      <ToggleWhiteBox title="Solutions" key={1}>
        {caseStudies.map((c) => {
          return (
            <p key={c}>
              <Link href="#">{c}</Link>
            </p>
          );
        })}
      </ToggleWhiteBox>
      <ToggleWhiteBox title="Referral Partners" key={2}>
        {caseStudies.map((c) => {
          return (
            <p key={c}>
              <Link href="#">{c}</Link>
            </p>
          );
        })}
      </ToggleWhiteBox>
      <ToggleWhiteBox title="Blog" key={3}>
        {caseStudies.map((c) => {
          return (
            <p key={c}>
              <Link href="#">{c}</Link>
            </p>
          );
        })}
      </ToggleWhiteBox>
    </div>
  );
};

const Links = (): ReactElement => {
  return (
    <div className="flex flex-row gap-5 mr-5">
      <div className="flex-none">
        <p className="font-bold text-payline-black">Case Study</p>
        {caseStudies.map((c) => {
          return (
            <p key={c}>
              <Link href="#">{c}</Link>
            </p>
          );
        })}
      </div>
      <div className="flex-none">
        <p className="font-bold text-payline-black">Solutions</p>
        {caseStudies.map((c) => {
          return (
            <p key={c}>
              <Link href="#">{c}</Link>
            </p>
          );
        })}
      </div>
      <div className="flex-none">
        <p className="font-bold text-payline-black">Referral Partners</p>
        {caseStudies.map((c) => {
          return (
            <p key={c}>
              <Link href="#">{c}</Link>
            </p>
          );
        })}
      </div>
      <div className="flex-none">
        <p className="font-bold text-payline-black">Blog</p>
        {caseStudies.map((c) => {
          return (
            <p key={c}>
              <Link href="#">{c}</Link>
            </p>
          );
        })}
      </div>
    </div>
  );
};

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
function Footer({ showBackground = true, showCopyright = true }): ReactElement {
  return (
    <>
      <div className="hidden lg:block">
        <div
          className={`${
            showBackground ? "bg-blog-search-footer bg-cover bg-no-repeat" : ""
          } px-8 py-16 b-10 h-490`}>
          <div className=" flex flex-row">
            <div className="flex-grow ml-5">
              <div className="mb-16">
                <Image
                  src="/images/payline-logo.svg"
                  layout="fixed"
                  height="32px"
                  width="113.27px"
                  alt="Payline Logo"
                />
              </div>
              <p>Flexible and Friendly Payment Processing Solutions</p>
              <div className="flex gap-2 py-2">
                <Link href="/terms-of-service">
                  <span className="font-opensans text-payline-black">
                    Terms of Service
                  </span>
                </Link>
                |
                <Link href="/privacy-policy">
                  <span className="font-opensans text-payline-black">
                    Privacy Policy
                  </span>
                </Link>
              </div>
              <div className="flex flex-row gap-3 py-3">
                <LinkedInIcon />
                <TwitterIcon />
              </div>
              <div className="flex flex-row gap-3">
                <GoogleIcon />
                <InstagramIcon />
              </div>
            </div>
            <Links />
          </div>
          {showCopyright && (
            <div
              className={`${
                showBackground ? "bg-footer-top bg-cover bg-no-repeat" : ""
              } pb-10 h-full`}>
              <p
                id="footer-left"
                className="text-xs font-opensans leading-loose text-payline-dark">
                Payline Data Services, LLC is an ISO of Wells Fargo Bank N.A.,
                Concord, CA
              </p>
              <p
                id="footer-right"
                className="text-xs font-opensans text-right text-payline-disabled mr-5">
                Copyright © {new Date().getFullYear()} Payline Data Services,
                LLC
              </p>
            </div>
          )}
        </div>
      </div>
      <div className="lg:hidden bg-footer-mobile bg-cover bg-no-repeat pt-12">
        <div className="px-6">
          <Image
            src="/images/payline-logo.svg"
            height="32px"
            width="113.27px"
            alt="Payline Logo"
          />
        </div>
        <p className="px-3 py-5 text-xl font-hkgrotesk leading-normal font-medium text-payline-black px-6">
          Flexible and Friendly Payment Processing Solutions
        </p>
        <div className="flex flex-row gap-3 py-5 px-6">
          <LinkedInIcon />
          <TwitterIcon />
          <GoogleIcon />
          <InstagramIcon />
        </div>
        <MobileLinks />
        <div className="flex gap-2 py-2 px-6">
          <Link href="/terms-of-service">
            <span className="font-opensans text-payline-black">
              Terms of Service
            </span>
          </Link>
          |
          <Link href="/privacy-policy">
            <span className="font-opensans text-payline-black">
              Privacy Policy
            </span>
          </Link>
        </div>
        {showCopyright && (
          <>
            <div className="py-6 px-6 h-full">
              <p className="font-opensans leading-loose text-payline-dark">
                Payline Data Services, LLC is an ISO of Wells Fargo Bank N.A.,
                Concord, CA
              </p>
            </div>
            <div className="h-full bg-payline-black flex align-center py-6 px-6">
              <p className="text-xs text-white font-opensans">
                Copyright © {new Date().getFullYear()} Payline Data Services,
                LLC
              </p>
            </div>
          </>
        )}
      </div>
    </>
  );
}

export default Footer;
