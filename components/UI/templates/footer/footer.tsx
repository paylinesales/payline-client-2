import React from "react";
import { useBusinessCategory } from "context/BusinessCategoryContext";
import FooterBrandCol from "./footerBrandCol";
import FooterCategory from "./FooterCategory";
import { PAYMENT_TYPES } from "../../../../constants";

const { ONLINE } = PAYMENT_TYPES;
// import Accordion, { AccordionItem } from "../../atoms/Accordions";

const siteMap = [
  {
    title: "case study",
    links: [
      {
        name: "Medical",
        href: "#",
      },
      { name: "OmniChannel", href: "#" },
      { name: "Hub And Spoke", href: "#" },
      { name: "Franchises", href: "#" },
      {
        name: "High Risk",
        href: "#",
      },
      { name: "eCommerce", href: "#" },
    ],
  },
  {
    title: "solutions",
    links: [
      {
        name: "POS Systems",
        href: "https://paylinedata.com/point-of-sale-systems/",
      },
      {
        name: "Developer Resources",
        href: "https://paylinedata.com/gateway-payment-processing/",
      },
      {
        name: "Payline Medical",
        href: "https://paylinedata.com/healthcare-credit-card-processing/",
      },
      {
        name: "Nonprofit/Education",
        href: "https://paylinedata.com/nonprofit-payment-processing/",
      },
      {
        name: "High Risk Payments",
        href: "https://paylinedata.com/high-risk-merchant-account/",
      },
      {
        name: "Business Loan",
        href: "https://paylinedata.com/small-business-loan/",
      },
    ],
  },
  {
    title: "referral partners",
    links: [
      { name: "Marketers", href: "#" },
      { name: "Content Creators", href: "#" },
      { name: "Developers", href: "#" },
      { name: "Lawyers", href: "#" },
      { name: "Accountants", href: "#" },
      { name: "Bookkeepers", href: "#" },
    ],
  },
  {
    title: "blog",
    links: [
      { name: "In-Person Payments", href: "#" },
      { name: "Online Payments", href: "#" },
      { name: "Payment Industry", href: "#" },
    ],
  },
];

const Footer: React.FC = () => {
  const { paymentType } = useBusinessCategory();
  // for as long as the payment type is on line then the business is online
  const isOnlineBusiness = paymentType === ONLINE;
  const sectionClass = isOnlineBusiness
    ? "bg-footer-online"
    : "bg-footer-in-person";
  return (
    <section id="footer" className={`bg-cover  bg-right-top ${sectionClass}`}>
      <div className="container flex flex-col lg:grid lg:grid-cols-6 mx-auto my-8 lg:pt-12 xl:pt-18 px-8 md:px-16 lg:px-20 xl:px-9">
        <FooterBrandCol className="lg:col-span-2" />
        <div className="lg:col-span-4 flex flex-col lg:flex-row justify-around w-full">
          {siteMap.map((item) => {
            return <FooterCategory key={item.title} {...item} />;
          })}
        </div>
      </div>
      <div className="bg-footer-bottom bg-cover bg-center py-14">
        <div className="container px-8 md:px-16 lg:px-20 xl:px-9 mx-auto text-sm flex items-center">
          <div className="xl:w-8/12 lg:w-9/12 text-payline-dark  hidden lg:block">
            <p className="mb-2">
              Payline Data Services, LLC is an ISO of Wells Fargo Bank N.A.,
              Concord, CA
            </p>
            <p>
              Payline Data Services LLC is a registered ISO for Fifth Third
              Bank, N.A. Cincinnati, OH, USA
            </p>
          </div>
          <div className="xl:w-4/12 lg:w-3/12 text-payline-disabled text-right  hidden lg:block lg:ml-10 xl:ml-0">
            <p>
              © {new Date().getFullYear()} Payline Data Services, LLC. All right
              reserved.
            </p>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Footer;
