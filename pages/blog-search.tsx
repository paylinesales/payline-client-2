import React, { useState } from "react";
import BlogLayout from "@/components/UI/layouts/BlogLayout";
import SearchBox from "@/components/UI/templates/sections/blog-search/SearchBox";
import BlogArticlesSection from "@/components/UI/templates/sections/blog-search/BlogArticlesSection";
import Footer from "@/components/UI/templates/sections/blog-search/Footer";

const BlogSearch = (props) => {
  /* storing state here as I need to change the state in the grandchildren of this funciton (children of SearchBox) 
    and use the state to determine which articles BlogArticlesSection should use */
  let { category, blogSearch } = props;
  if (typeof category === "string") {
    category = [category];
  } else {
    category = [];
  }
  const [categories, setCategories] = useState<string[]>(category);
  if (typeof blogSearch !== "string") {
    blogSearch = "";
  }
  const [search, setSearch] = useState(blogSearch);
  const [loading, setLoading] = useState(null);
  const [error, setError] = useState(null);
  const [data, setData] = useState(null);
  return (
    <BlogLayout>
      <div className="bg-blog-search bg-cover bg-repeat">
        <div className="mt-16 md:px-12 px-2">
          <SearchBox
            setCategories={setCategories}
            categories={categories}
            search={search}
            setSearch={setSearch}
            querySetters={{ setLoading, setError, setData }}
            queryResult={{ loading, error, data }}
          />
        </div>
      </div>

      <BlogArticlesSection
        categories={categories}
        setCategories={setCategories}
        search={search}
        queryResult={{ loading, error, data }}
      />

      <Footer showCopyright={false} />
    </BlogLayout>
  );
};

BlogSearch.getInitialProps = ({ query }) => {
  return {
    blogSearch: query.search,
    category: query.category,
  };
};

export default BlogSearch;
