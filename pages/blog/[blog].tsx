/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import moment from "moment";
import Image from "next/image";
import { useRouter } from "next/dist/client/router";
import { ReactElement } from "react";
import WhiteButton from "@/components/UI/atoms/WhiteButton";
import BlogLayout from "@/components/UI/layouts/BlogLayout";
import CostAnalysisSection from "@/components/UI/templates/sections/blog/[slug]/CostAnalysisSection";
import client from "@/lib/apolloClient";
import { blogArticleQuery } from "@/queries/blog";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const Author = ({ post }) => {
  return (
    <div className="mt-6 px-8 flex items-center">
      <div className="flex-shrink-0">
        <a href={post?.author?.href}>
          <span className="sr-only">{post?.author?.name}</span>
          <img
            className="h-10 w-10 rounded-full"
            src={post?.author?.imageUrl}
            alt={post?.author?.name}
          />
        </a>
      </div>
      <div className="ml-3">
        <p className="text-sm font-medium text-gray-900">
          <a href={post?.author?.href} className="hover:underline">
            {post?.author?.name}
          </a>
        </p>
      </div>
    </div>
  );
};

const Category = ({ category }) => {
  return (
    <div>
      <WhiteButton className="rounded-full">
        <span className="px-4">{category.node.name}</span>
      </WhiteButton>
    </div>
  );
};

export default function BlogTemplate({ post }): ReactElement {
  const router = useRouter();

  if (!router.isFallback && !post?.slug) return <span>404 error</span>;

  return (
    <BlogLayout>
      <div className="w-screen mt-16 px-4 lg:px-24 xl:px-52">
        {/* TODO: Get Online vs. In-Person to replace date for mobile */}
        {/* Date & Request Callback - Only displays for desktop */}
        <div className="hidden md:flex justify-between">
          <div className="flex items-end my-3 text-payline-peach">
            {moment(post.date).format("DD MMMM YYYY")}
          </div>

          <div className="flex">
            <Image src="/images/svg/blog-woman.svg" width={72} height={72} />
            <div className="flex items-center -ml-5">
              <button
                type="button"
                className="flex items-center gap-3 px-8 rounded-full bg-payline-dark text-white text-payline-dark p-3">
                <div>Request a Callback</div>
                <div>
                  <Image
                    src="/images/svg/chevron-right.svg"
                    width={6}
                    height={12}
                  />
                </div>
              </button>
            </div>
          </div>
        </div>

        {/* Blog Header */}
        <div className="flex flex-col w-full md:flex-row items-center justify-center space-y-5 md:space-x-2 lg:space-x-5 mt-5">
          <div className="w-screen flex flex-col justify-center">
            {/* <div className="hidden md:block my-3">
              <Author post={post} />
            </div> */}
            <div className="px-4 mt-4 md:mt-0 md:px-0 md:pr-8">
              <h1 className="font-bold text-2xl md:text-3xl lg:text-4xl">
                {post.title}
              </h1>
            </div>
            <div className="hidden md:flex md:flex-row md:flex-wrap space-x-2 my-5">
              {post.categories &&
                post.categories.edges.map((category) => (
                  <Category category={category} />
                ))}
            </div>
          </div>
          <div className="hidden md:relative md:block md:mr-8">
            <Image
              className="hidden sm:relative"
              src={
                post.featuredImage
                  ? post.featuredImage.node.url
                  : "/images/defaultBlogPic.jpg"
              }
              alt="Featured Image"
              layout="intrinsic"
              width={500}
              height={350}
            />
          </div>
          <div className="relative block w-screen h-60 xs:h-80 sm:h-96 md:hidden ">
            <Image
              className="relative px-2 md:hidden"
              src={
                post.featuredImage
                  ? post.featuredImage.node.url
                  : "/images/defaultBlogPic.jpg"
              }
              alt="Featured Image"
              layout="fill"
              objectFit="contain"
            />
          </div>
          {/* <div className="relative block w-screen h-48 sm:hidden">
            <Image
              className="relative md:hidden"
              src={
                post.featuredImage
                  ? post.featuredImage.node.url
                  : "/images/defaultBlogPic.jpg"
              }
              alt="Featured Image"
              layout="fill"
              objectFit="cover"
            />
          </div> */}
        </div>

        {/* Blog Content */}
        <div
          // eslint-disable-next-line react/no-danger
          dangerouslySetInnerHTML={{ __html: post.content }}
          className="pt-2 sm:pt-10 md:bg-blog-content md:bg-cover md:bg-no-repeat"
        />
      </div>

      {/* <div className="md:hidden">
        <Author post={post} />
        <Category category={post} />
      </div> */}
      {/* <div className="flex justify-center items-center">
        {post.featuredImage && (
          <Image
            src={post.featuredImage ? post.featuredImage.node.url : ""}
            width={600}
            height={400}
          />
        )}
      </div> */}
      {/* Blog Content */}
      <CostAnalysisSection />
    </BlogLayout>
  );
}

// removed getStaticProps from here and added
// getServerSideProps because this is SSR application.
export async function getServerSideProps({ params }) {
  // params contains the blog `id` or `slug` in this case.
  // If the route is like /blog/payline, then params.blog is payline
  const { data } = await client.query({
    query: blogArticleQuery,
    variables: {
      id: params.blog,
    },
  });

  // Pass  data to the page via props
  return { props: { post: data.post } };
}
