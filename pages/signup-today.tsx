/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
// import BlogLayout from "@/components/UI/layouts/BlogLayout";
import Head from "next/head";
import client from "lib/apolloClient";
import SignUpForm from "@/components/UI/templates/sections/sign-up/SignUpForm";
import signUpQuery from "../queries/signUp";

export default function SignUp({ signUpData }) {
  const parsedSignUpData = JSON.parse(signUpData);
  const destructuredData = parsedSignUpData.data.pages.edges[0].node;

  const {
    seo,
    date,
    signUpPage: {
      backgroundImage: {
        altText: backgroundImageAltText,
        sourceUrl: backgroundImageSourceUrl,
        title: backgroundImageTitle,
      },
      foregroundImage: {
        altText: foregroundImageAltText,
        sourceUrl: foregroundImageSourceUrl,
        title: foregroundImageTitle,
      },
      headline: {
        part1: headlinePart1,
        part2: headlinePart2,
        part3: headlinePart3,
        part4: headlinePart4,
        part5: headlinePart5,
      },
      hubspotForm,
      subtleCta,
    },
    title,
  } = destructuredData;
  return (
    <div className="md:grid md:grid-cols-2 flex flex-col-reverse bg-mobile-sign-up md:bg-none bg-cover bg-no-repeat">
      <Head>
        <title>{title}</title>
      </Head>
      <div
        className="hidden md:block"
        style={{
          background: `url(${backgroundImageSourceUrl})`,
          backgroundSize: "cover",
          backgroundRepeat: "no-repeat",
          height: "100%",
        }}>
        <div className="flex md:flex-col justify-center items-center h-screen relative">
          <p className="text-payline-peach absolute top-40 place-self-start left-40">
            {subtleCta}
          </p>
          <img src={foregroundImageSourceUrl} height="100" alt="" />
        </div>
      </div>
      <div className="flex flex-col mt-12 md:mt-16 lg:mx-20">
        <div className="flex flex-col text-center text-3xl justify-center py-8">
          <p>
            {headlinePart1}{" "}
            <span className="px-2 font-hkgrotesk leading-relaxed md:leading-loose font-bold text-payline-white bg-payline-cta">
              {headlinePart2}
            </span>{" "}
            {headlinePart3}
          </p>
          <p>
            <span className="px-2 font-hkgrotesk leading-relaxed md:leading-loose font-bold text-payline-white bg-payline-cta">
              {headlinePart4}
            </span>
            {headlinePart5}
          </p>
        </div>
        <SignUpForm />
      </div>
    </div>
  );
}

export async function getServerSideProps() {
  const signUp = await client.query({
    query: signUpQuery,
  });

  const signUpData = JSON.stringify(signUp);
  return {
    props: { signUpData },
  };
}
