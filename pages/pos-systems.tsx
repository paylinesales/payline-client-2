/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import client from "lib/apolloClient";
import * as React from "react";
import Head from "next/head";
import BlogSection from "components/UI/templates/sections/home/BlogSection/BlogSection";
import LetsGetInTouchAnalysis from "components/UI/templates/sections/pos-system/LetsGetInTouchAnalysis/LetsGetInTouchAnalysisSection";
import LogoBar from "components/UI/organisms/banners/LogoBar";
import MainHero from "components/UI/templates/sections/home/mainHeroSection";
import PageLayout from "@/components/UI/layouts/PageLayout";
import BetterWaySendMoney from "@/components/UI/templates/sections/pos-system/BetterWaySendMoney";
import RetailPaymentProcessing from "@/components/UI/templates/sections/pos-system/RetailPaymentProcessing";
import OurProcess from "@/components/UI/templates/sections/pos-system/OurProcess";
import pointOfSaleQuery from "../queries/posSystems";

interface PosSystemProps {
  pointOfSaleQueryData: string;
}

const PosSystem: React.FC<PosSystemProps> = ({ pointOfSaleQueryData }) => {
  console.log(pointOfSaleQueryData);
  const parsedData = JSON.parse(pointOfSaleQueryData);
  // https://wesbos.com/destructuring-renaming
  const { pages: destructuredPagedData, websiteCopy } = parsedData.data;
  const { logoBar } = websiteCopy;
  // const { logoBar } = websiteCopy;
  const {
    heroSection,
    title,
    betterWaySendMoney,
    ourProcess,
    retailPaymentProcessing,
  } = destructuredPagedData.edges[0].node;

  return (
    <PageLayout>
      <Head>
        <title>{title}</title>
      </Head>
      {/* dynamic section: MainHero */}
      <MainHero data={heroSection} />
      <LogoBar logoBar={logoBar} />
      <div className="pos-wrapper">
        {/* <LogoBar logoBar={logoBar} /> */}
        <BetterWaySendMoney betterWaySendMoney={betterWaySendMoney} />
        <OurProcess ourProcess={ourProcess} />
        <RetailPaymentProcessing
          retailPaymentProcessing={retailPaymentProcessing}
        />
      </div>
      <BlogSection />
      <LetsGetInTouchAnalysis />
    </PageLayout>
  );
};

export async function getServerSideProps() {
  const pointOfSale = await client.query({
    query: pointOfSaleQuery,
    variables: {
      title: "Point of Sale Systems",
    },
  });

  const pointOfSaleQueryData = JSON.stringify(pointOfSale);

  return {
    props: { pointOfSaleQueryData },
  };
}

export default PosSystem;
