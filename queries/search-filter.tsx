import { gql } from "@apollo/client";

const searchFilterQuery = gql`
  query searchFilterQuery {
    categories(first: 10000) {
      nodes {
        name
        slug
      }
    }
  }
`;

export default searchFilterQuery;
