import { gql } from "@apollo/client";

const pointOfSaleQuery = gql`
  query PageQuery($title: String!) {
    pages(where: { title: $title }) {
      edges {
        node {
          title
          slug
          heroSection {
            fieldGroupName
            headline {
              leadingText
              highlightedText
            }
            backgroundImage {
              sourceUrl
              title
            }
          }
          betterWaySendMoney {
            bulletPointsLeftColumnMoney {
              bulletTextMoney
              fieldGroupName
            }
            sectionHeaderMoney {
              fieldGroupName
              largeHeaderMoney
              smallHeaderMoney
            }
            paragraphTwoRightColumn
            paragraphTwoLeftColumn
            paragraphThreeRightColumn
            paragraphThreeLeftColumn
            paragraphOneRightColumn
            paragraphOneLeftColumn
            fieldGroupName
          }
          ourProcess {
            fieldGroupName
            bulletPointsProcess {
              bulletTextProcess
              fieldGroupName
            }
            leftColumnImage {
              sourceUrl
            }
            paragraphFive
            paragraphFour
            paragraphOne
            paragraphSeven
            paragraphSix
            paragraphThree
            paragraphTwo
            subheading
            sectionHeaderProcess {
              largeHeaderProcess
              smallHeaderProcess
            }
          }
          retailPaymentProcessing {
            fieldGroupName
            bulletPointsRetail {
              bulletTextRetail
              fieldGroupName
            }
            rightColumnImage {
              sourceUrl
            }
            paragraphFive
            paragraphFour
            paragraphOne
            paragraphThree
            paragraphTwo
            sectionHeaderRetail {
              largeHeaderRetail
              smallHeaderRetail
            }
          }
        }
      }
    }
    websiteCopy {
      logoBar {
        logoGroup {
          logo {
            sourceUrl
          }
        }
      }
    }
  }
`;

export default pointOfSaleQuery;
